var game = new Phaser.Game(800, 600, Phaser.CANVAS, 'phaser-example', { preload: preload, create: create, update: update, render: render });

function preload() {
    // game.load.atlasJSONHash('bot-walk', 'assets/sprites/running_bot.png', 'assets/sprites/running_bot.json');
    game.load.spritesheet('bot-walk', 'assets/sprites/ff3_walk.png', 51, 99);
    game.load.spritesheet('bot-idle', 'assets/sprites/ff3_idle.png', 60, 99);
    game.load.image('back', 'assets/backgrounds/back1.png');
}

var s;

function create() {
    var b = game.add.sprite(0, 0, 'back');
    b.width = 2425;
    b.height = 600;
    s = game.add.sprite(game.world.centerX, game.world.centerY, 'bot-idle');
    s.anchor.setTo(.5,.5);
    s.scale.setTo(2, 2);
    animation.setStatus('idle');
}
function moveX(s, delta){
    s.x+=delta;
}
function moveY(s, delta){
    s.y+=delta;
}

function flipX(s, direction){
    if ((direction<0 && s.scale.x<0) || ( direction>0 && s.scale.x>0))
        s.scale.x*=-1;
}

var animation = {
    idle: function(){
        s.statusAnim = 'idle';
        s.statusChar = 'idle';
        s.loadTexture('bot-idle', 0);
        s.animations.add('idle');
        s.animations.play('idle', 4, true);
    },
    walk: function(){
        s.statusAnim = 'walk';
        s.statusChar = 'walk';
        s.loadTexture('bot-walk', 0);
        s.animations.add('walk');
        s.animations.play('walk', 12, true);
        s.isanimation = true;
    },
    
    loop: function(){
        if (s.statusChar!=s.statusAnim){
            switch (s.statusChar){
                case 'walk':
                    animation.walk();
                    break;
                case 'idle':
                    animation.idle();
                    break;
            }
        }
    },
    
    setStatus: function(status){
        s.statusChar = status;
        animation.loop();
    }
}

function update() {
    var delta = 3;
    if (game.input.keyboard.isDown(Phaser.Keyboard.LEFT))
    {
        flipX(s, 1);
        moveX(s, -delta);
        animation.setStatus('walk');
    }
    else if (game.input.keyboard.isDown(Phaser.Keyboard.RIGHT))
    {
        flipX(s, -1);
        moveX(s, delta);
        animation.setStatus('walk');
    }


    if (game.input.keyboard.isDown(Phaser.Keyboard.UP))
    {
        moveY(s, -delta);
        animation.setStatus('walk');
    }

    else if (game.input.keyboard.isDown(Phaser.Keyboard.DOWN))
    {
        moveY(s, delta);
        animation.setStatus('walk');
    }

    if (s.statusChar=='idle'){
        animation.setStatus('idle');
    }
    
    s.statusChar='idle';

}

function render() {
    // game.debug.spriteInfo(s, 20, 32);

}